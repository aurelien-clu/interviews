
class Node:

    def __init__(self, value, left=None, right=None):

        # to raise all initialisation issues at once
        error_msg = []

        # setting node value
        if not isinstance(value, int):
            error_msg.append('value={}, it should be an integer'.format(value))
        else:
            self.value = value

        # setting left and right nodes unless it is a leaf
        if left is not None and right is not None:
            self.left = left
            self.right = right
            self.is_leaf = False
        elif left is None and right is None:
            self.is_leaf = True
        else:
            error_msg.append(
                'left={}, right={}, both should be None (leaf) or none should be None.'.format(left, right))

        if len(error_msg) > 0:
            raise ValueError('\n\t' + '\n\t'.join(error_msg))

    def __repr__(self):
        return str(self.__dict__)

    def get_max_value(self):
        if self.is_leaf:
            return self.value
        else:
            return max([self.value, self.left.get_max_value(), self.right.get_max_value()])
