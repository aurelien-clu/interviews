# Exercises made for a firm specialised in the creation of Chatbots

Both exercises were done at home, obviously alone and in less than an hour.
Exercises being quite simplistic so is the code and documentation.

## Code exercise

### Goal

> Build a balanced binary tree with each node holding an integer value.
> 
> The root node should provide a way of retrieving the maximum value of the tree.


### Getting started

#### Prerequisites

Install Python3.

#### Running tests

```bash
cd code_exercise
python3 -m unittest
```

## Data science exercise

### Goal

> Make a classifier with a F1-score above 0.85.

### Libraries

> Use Scikit-learn classification report for the performance
> 
> Use any library or tool you see fit

### Getting started

#### Prerequisites

```bash
# install python 3
# make a virtual environment if necessary
```

```bash
cd data_science_exercise
# install necessary packages
pip3 install -r requirements.txt
```

#### Running the code

```python
python3 src/classifier.py
```
