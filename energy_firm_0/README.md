# Energy firm 0

## Self evaluation

Given the following table:

|Grade|Level|
|---|---|
|0|impossible, hearing the name of a topic gives more than 0|
|1|limited knowledge about it|
|2|small developments|
|3|develop using documentation|
|4|develop autonomously with 10% of the documentation|
|5|recognized expert, able to teach to level 3|
|6|non official publications|
|7|PHD + official publications|
|8|recognized nationally|
|9|recognized internationally|
|10|impossible, there are always room for improvements|

grade yourself on the following topics:

|Topic|personal grade (2017-11)|
|---|---|
|Python|3.5|
|Scipy|3.5|
|Keras|3.5|
|PyTorch|2.5|
|||
|Scala|3.5|
|Hadoop|3.5|
|Spark|4.0|
|||
|Machine learning|3.5|

## Coding interview

Python exercise, no recall.
