# Exercise made for a firm supplying a software suite for the Google Cloud platform

The exercise was made on site without internet (wifi issues) within around 15 minutes.

## Goal

> Find the top 5 rows (count wise).

## Getting started

### Prerequisites

Install Python 3 Jupyter Notebook.

### Running the code

Load the notebook and execute all cells.
