# Exercise made for a firm specialised in Human Resource services

## Summary

* The exercise
    * Goal of the exercise
    * Context
    * Objectives
    * Note
    * Outputs
        * industry_top50.txt
        * company_top50.txt
    * Constraints

* My solution
    * Train of thought
    * Getting started
        * Prerequisites
        * Running the code

## The exercise

### Goal of the exercise

Determine the skills of a candidate to handle efficiently a big amount of data 
and to extract information/insights from it.

Scientific questions are to come in a next interview.

### Context

A file named job_post.log contains identified job offers with the format explained below.

Each line contains a different job offer.

Data format is as follows:

|job title|industry_id|company_id|
|:---     |:---       |:---      |

With:
* job title: The job name
* industry_id: industry name, contains only lowercase letters and underscores
* company_id: an int referring to the firm who has posted the offer

For the exercise we will consider that:
    
    there are around 30 millions different companies
    there are around 250 different industries
    there are around 10 million different job names
    the file size is around 1 To.

One should expect the file to have corrupt lines, i.e. not respecting the expected format,
the script should be robust to such case.

### Objectives

You should propose a system computing:
* the top 50 jobs per industry
* the top 50 jobs per company

### Note

Job names are voluntarily not normalized (errors, synonyms, formatting, etc.)
The candidate should think of a solution to improve this depending on his available time.

### Ouputs

#### industry_top50.txt

Each row should contain the top 50 jobs per industry with the following format:

    industry_id|job1:n1,job2:n2,job3:n3,...,job50:n50

where job1:n1 the job with the most asked offers and the number of offers associated
etc.

The file should be sorted by ascending industry_id.

#### company_top50.txt

Each row should contain the top 50 jobs per company_id with the following format:

    company_id|job1:n1,job2:n2,job3:n3,...,job50:n50

Same as before with industry_id being replaced by company_id.

The file should be sorted by ascending company_id.

### Constraints

To solve this, you should use the language you see fit (shell, PHP, Python, Java, C/C++, Perl, etc..)
and can write on disk as many temporary files you want in the format you want.

However, you cannot use any third party system needing to run a service. (no MySQL, MongoDB, Hadoop, Spark, etc.)

The solution should not use too much memory (< 1GB), even if it implies disk writes. 
Data can be kept for the following day.

The solution should be easily readable and maintainable.

The expected work should take at least four hours.

## My solution

### Train of thought

A reward for the cool readers: [urbandictionary: train of thought](https://www.urbandictionary.com/define.php?term=train%20of%20thought)

#### Sum up

* big data
    * there are around 30 millions different companies
    * there are around 250 different industries
    * there are around 10 million different job names
    * the file size is around 1 TB.
* data is corrupted
* data is not normalized
* program should run under 1 GB memory
* program should resume on pause (data can be processed in several days)
* disk writes are accepted (obviously necessary for a 1 TB data)
* coded in at least 4 hours
* job field normalization is "the cherry on top"
* objectives
    * the top 50 jobs per industry
    * the top 50 jobs per company

#### Implementation

##### Data per pass

=> Local Map Reduce with a single worker having only 1 GB memory for a 1 TB problem

Provided data is around 32 MB, 685 217 rows.

Memory of the worker:
* 200 MB of program (worst case)
* 400 MB of data read
* 400 MB of count (worst case)

=> **in memory we should have at most 274 rows of data** (685217 / 1000 * 40%) 
to replicate the necessity of multiple passes on the data.
(Otherwise one load with Pandas and / or Scikit learn and it is done)

##### Architecture

Solution 1 - Hadoop Map Reduce like:

* Splitter: A process splits the input into data pieces
* Mapper: A Process maps 1 to each row
* Reducer: A process makes a "GroupBy - Count" on each mapped split
* Shuffle and sort: to be able to know whenever a key is fully counted or not
* Run the reducer + shuffle and sort until each key is fully counted

Each step is persisted on disk.

Solution 2 - Simpler Map Reduce using a self made "database":
* Splitter: A process splits the input into data pieces
* Mapper: A Process maps 1 to each row
* Reducer: A process makes a "GroupBy - Count" on each mapped split
* Updater: A process that updates a database with the past count + the newly counted values
* Run the reducer + updater until there are no more splits to process

Each step is persisted on disk.
The count is persisted into a self made database.


### Getting started

#### Prerequisites

#### Running the code

